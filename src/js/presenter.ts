// Presenter code hier
// Hier kan je DOM API aanroepen en html aanpassen
import {getPosts, deletePost, createPost} from "./restclient.js";
import {createSimpleElement} from "./util/DomUtil.js";
import Post from "./models/Post.js";

const POSTS_CONTENT = document.getElementById("content")!;
const POSTS_FORM = document.getElementById("post-form")!;
const TITLE_INPUT = document.getElementById("titel") as HTMLInputElement;
const CONTENT_INPUT = document.getElementById("inhoud") as HTMLTextAreaElement;
const FEEDBACK = document.getElementById("feedback") as HTMLTextAreaElement;
const FEEDBACK_BLOCK = document.getElementById("feedback-block") !;


function deletFromList(post: Post) {
  try {
    deletePost(post.id!);
    presentPosts();
  } catch (e) {
    showError(true, e);
  }
}

function createDeleteButton(post: Post): HTMLButtonElement {
  const button = createSimpleElement(
    "button",
    "Delete Post",
    ["btn", "btn-danger", "my-1"]
  ) as HTMLButtonElement;
  button.addEventListener("click", e => deletFromList(post));
  return button;
}

//        <article class="card p-0 m-2">
//         <div class="card-header ">Nieuws item 1</div>
//         <div class="card-body">
//           <h5 class="card-title">Post 1</h5>
//           <div>This is the content of post 1</div>
//         </div>
//       </article>

function showPost(post: Post): Node {
  const body = createSimpleElement("div", null, ["card-body"]);
  body.append(
    createSimpleElement("h5", post.title, ["card-title"]),
    createSimpleElement("div", post.content),
    createDeleteButton(post)
  );
  const postNode = createSimpleElement("article", null, ["card", "p-0", "m-2"]);
  postNode.append(
    createSimpleElement("div", "Nieuws item #" + post.id, ["card-header"]),
    body
  );
  return postNode;
}

function showPosts(posts: Post[]) {
  POSTS_CONTENT.replaceChildren(...posts.map(showPost));
}

async function presentPosts() {
  try {
    const posts = await getPosts();
    showError(false);
    showPosts(await getPosts());
  } catch (e) {
    showError(true, e);
  }
}

async function post(e: Event) {
  e.preventDefault();
  try {
    await createPost({
      title: TITLE_INPUT.value,
      content: CONTENT_INPUT.value
    });
    presentPosts();
    TITLE_INPUT.value = "";
    CONTENT_INPUT.value = "";
  } catch (e) {
    showError(true, e);
  }

}

function addEventListeners() {
  POSTS_FORM.addEventListener("submit", post)
}

export async function initPosts() {
  await presentPosts();
  addEventListeners();
}

function showError(show: boolean, e?: unknown) {
  FEEDBACK_BLOCK.hidden = !show;
  if (e) {
    FEEDBACK.textContent = e instanceof Error ? e.message : String(e);
  }
}