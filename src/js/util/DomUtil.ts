export function createSimpleElement(name:string,text?:string|null,classes?:string[]):HTMLElement{
  const element = document.createElement(name);
  if (text){
    element.innerText=text;
  }
  if(classes){
    element.classList.add(...classes);
  }
    return element;
}

