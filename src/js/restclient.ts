// REST code hier
// Hier ge je enkel REST call doen en data verwerken. Hier wordt geen html gemaakt/geschreven of aangepast.
import Post from "./models/Post.js";

const SERVER_URL = "http://localhost:3000"
const POSTS_URL = `${SERVER_URL}/posts`

export async function getPosts(): Promise<Post[]> {
  const response = await fetch(POSTS_URL);
  if (response.ok) {
    return response.json()
  } else {
    throw new Error("Problem getting data from Server")
  }
}

export async function deletePost(id: string) {
  const response = await fetch(`${POSTS_URL}/${id}`, {method: "DELETE"});
  if (!response.ok) throw new Error(`Cannot delete post with id ${id}`)
}

export async function createPost(post: Post) {
  const response = await fetch(`${POSTS_URL}`, {
    method: "POST",
    headers: {"Content-Type": "application/json"},
    body: JSON.stringify(post)
  });
  if (!response.ok) throw new Error(`Cannot create post ${post}`)
}